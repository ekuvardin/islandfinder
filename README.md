To build program you should have

    1. maven 3.5.0
    2. Java 8

How to run

    1. Using executable jar
        In the command line
            1.1 cd <Project folder>
            1.2 cd Finder
            1.3 java -jar target/Finder-1.0.jar -ld resources/test1.txt

    2. maven-exec-plugin
        In the command line
            2.1 cd <Project folder>
            2.2 cd Finder
            2.3 mvn exec:java -Dexec.args="-ld resources/test1.txt"

    To load some special files or use another algorithm replace arguments
    (all lines after -Dexec.args for maven-exec-plugin or all lines after target/Finder-1.0.jar for executable jar)
     with commands below

Available commands

    -ld - load data from file
        example
            -ld Finder/resources/test1.txt

    -a - type of algorithm to use
        Available types:
        R   - recursive                     (Finder\src\main\java\actions.findislands.FindIslandRecursive)
        WL  - scan line(default)            (Finder\src\main\java\actions.findislands.FindIslandGoThroughWidth)
        PR  - parallel recursive            (Finder\src\main\java\actions.findislands.FindParallels)
        PWL - parallel scan line            (Finder\src\main\java\actions.findislands.FindParallels)

        example
            -ld Finder/resources/test1.txt -a R

        !!!Warning!!!
        Recursive algorithm may fail due to stackoverflow error.
        To suppress this use additional JVM parameter -Xss1024m
        Or set block size small enough(See commands below)

    -bw - block size width(Used only in parallel algorithms)
        If block size width bigger than width or searched area them block size = block area width
        By default = Integer.MAX_VALUE

        example
            -ld Finder/resources/test1.txt -a PWL -bw 64

    -bh - block size height(Used only in parallel algorithms)
        If block size height bigger than height or searched area them block size height = block area height
        By default = Integer.MAX_VALUE

        example
            -ld Finder/resources/test1.txt -a PWL -bw 64 -bh 64

Finally when all checks were successfull answer should be:

    <<Founded islands are 3>>


Some additional info:

    Main class Finder\src\main\java\runner\simpleEditor

Algorithms

    Finder\src\main\java\actions.findislands.FindIslandRecursive
    Finder\src\main\java\actions.findislands.FindIslandGoThroughWidth
    Finder\src\main\java\actions.findislands.FindParallels

Integration tests

    Finder\src\test\integration

Unit tests

    Finder\src\test\unit

Benchmarks

    Benchmark\src\main\java\findisland