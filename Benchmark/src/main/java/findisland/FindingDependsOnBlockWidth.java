package findisland;

import actions.AvailableSymbols;
import actions.findislands.FindIslandGoThroughWidth;
import actions.findislands.FindParallels;
import actions.findislands.IFindIsland;
import model.Model;
import model.SimpleModel;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;
import view.ConsoleViewer;
import view.IView;

import java.util.concurrent.TimeUnit;

/**
    As you can see as bigger width the more effective algorithm.
    I suppose it's because array in memory allocated consistently and going on width
    processor can prefetch and predict our next stage
    When we going on height next element will be current + width which more complicated and needs more CPU
    and sophisticated optimization

    Look at ParallelFindingGoWidth5000Width and compare with single thread analogue in {@link FindingFullEarthBigArea}
    Now it has more than 33,628/9,320 ~ 3.6 speed up

    Benchmark                                                                   Mode  Cnt   Score   Error  Units
    findisland.FindingDependsOnBlockWidth.ParallelFindingGoWidth256Width.find   avgt    5  11,544 ± 0,699  ms/op
    findisland.FindingDependsOnBlockWidth.ParallelFindingGoWidth5000Width.find  avgt    5   9,320 ± 0,383  ms/op
    findisland.FindingDependsOnBlockWidth.ParallelFindingGoWidth64Width.find    avgt    5  12,048 ± 0,939  ms/op
 */
public class FindingDependsOnBlockWidth {

    static int width = 5000;
    static int height = 5000;

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    @Fork(1)
    public static class ParallelFindingGoWidth64Width {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            IView view = new ConsoleViewer();
            findIsland = new FindParallels(view, model, new FindIslandGoThroughWidth(model), 4,64,64);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    @Fork(1)
    public static class ParallelFindingGoWidth256Width {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            IView view = new ConsoleViewer();
            findIsland = new FindParallels(view, model, new FindIslandGoThroughWidth(model), 4,256,16);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    @Fork(1)
    public static class ParallelFindingGoWidth5000Width {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            IView view = new ConsoleViewer();
            findIsland = new FindParallels(view, model, new FindIslandGoThroughWidth(model), 4,5000,4);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    private static Model fillModel(){
        SimpleModel model = new SimpleModel(width, height);

        for (int i = 0; i < model.getMaxHeight(); i++)
            for (int j = 0; j < model.getMaxWidth(); j++)
                model.set(j, i, AvailableSymbols.EARTH.getValue());

        return model;
    }

    public static void main(String[] args) {

        Options opt = new OptionsBuilder()
                .include(FindingDependsOnBlockWidth.class.getSimpleName())
                .warmupIterations(4)
                .measurementIterations(5)
                .forks(1)
                .timeout(TimeValue.seconds(60))
                .jvmArgs("-ea")
                .build();
        try {
            new Runner(opt).run();
        } catch (RunnerException e) {
            e.printStackTrace();
        }
    }
}
