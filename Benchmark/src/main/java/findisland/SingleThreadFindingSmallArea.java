package findisland;

import actions.AvailableSymbols;
import actions.findislands.FindIslandGoThroughWidth;
import actions.findislands.FindIslandRecursive;
import actions.findislands.IFindIsland;
import model.Model;
import model.SimpleModel;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

import java.util.concurrent.TimeUnit;

/*
    Simple bench comparing work on small area

    Benchmark                                                      Mode  Cnt      Score      Error  Units
    findisland.SingleThreadFindingSmallArea.FindingGoWidth.find    avgt    5  10490,957 ±  170,620  ns/op
    findisland.SingleThreadFindingSmallArea.RecursiveFinding.find  avgt    5  90223,644 ± 1498,016  ns/op
 */
public class SingleThreadFindingSmallArea {

    static int width = 128;
    static int height = 128;

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    @Fork(jvmArgsAppend = "-Xss1024m")// Suppress stackoverflow
    public static class RecursiveFinding {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            findIsland = new FindIslandRecursive(model);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    @Fork(1)
    public static class FindingGoWidth {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            findIsland = new FindIslandGoThroughWidth(model);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    private static Model fillModel(){
        SimpleModel model = new SimpleModel(width, height);

        for (int i = 0; i < model.getMaxHeight(); i++)
            for (int j = 0; j < model.getMaxWidth(); j++)
                model.set(j, i, AvailableSymbols.WATER.getValue());

        for (int i = 0; i < height; i = i + 5)
            for (int j = 0; j < width; j = j + 5)
                model.set(j, i, AvailableSymbols.EARTH.getValue());

        return model;
    }

    public static void main(String[] args) {

        Options opt = new OptionsBuilder()
                .include(SingleThreadFindingSmallArea.class.getSimpleName())
                .warmupIterations(4)
                .measurementIterations(5)
                .forks(1)
                .timeout(TimeValue.seconds(60))
                .jvmArgs("-ea")
                .build();
        try {
            new Runner(opt).run();
        } catch (RunnerException e) {
            e.printStackTrace();
        }
    }

}
