package findisland;

import actions.AvailableSymbols;
import actions.findislands.FindIslandGoThroughWidth;
import actions.findislands.FindIslandRecursive;
import actions.findislands.FindParallels;
import actions.findislands.IFindIsland;
import model.Model;
import model.SimpleModel;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;
import view.ConsoleViewer;
import view.IView;

import java.util.concurrent.TimeUnit;

/*
    In this situation ParallelRecursiveFinding gives the best performance(The single one in all bench)
    It can be recommend to use in likely empty area

    Benchmark                                                         Mode  Cnt   Score   Error  Units
    findisland.FindingEmptyBigArea.ParallelFindingGoWidth.find        avgt    5   7,322 ± 0,465  ms/op
    findisland.FindingEmptyBigArea.ParallelRecursiveFinding.find      avgt    5   7,113 ± 0,706  ms/op

    findisland.FindingEmptyBigArea.SingleThreadFindingGoWidth.find    avgt    5  11,637 ± 0,493  ms/op
    findisland.FindingEmptyBigArea.SingleThreadRecursiveFinding.find  avgt    5  21,457 ± 0,376  ms/op
 */
public class FindingEmptyBigArea {

    static int width = 5000;
    static int height = 5000;

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    public static class SingleThreadRecursiveFinding {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            IView view = new ConsoleViewer();
            findIsland = new FindParallels(view, model, new FindIslandRecursive(model), 1,64,64);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    @Fork(1)
    public static class SingleThreadFindingGoWidth {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            findIsland = new FindIslandGoThroughWidth(model);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    public static class ParallelRecursiveFinding {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            IView view = new ConsoleViewer();
            findIsland = new FindParallels(view, model, new FindIslandRecursive(model), 4,64,64);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    @Fork(1)
    public static class ParallelFindingGoWidth {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            IView view = new ConsoleViewer();
            findIsland = new FindParallels(view, model, new FindIslandGoThroughWidth(model), 4,64,64);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    private static Model fillModel(){
        SimpleModel model = new SimpleModel(width, height);

        for (int i = 0; i < model.getMaxHeight(); i++)
            for (int j = 0; j < model.getMaxWidth(); j++)
                model.set(j, i, AvailableSymbols.WATER.getValue());

        return model;
    }

    public static void main(String[] args) {

        Options opt = new OptionsBuilder()
                .include(FindingEmptyBigArea.class.getSimpleName())
                .warmupIterations(4)
                .measurementIterations(5)
                .forks(1)
                .timeout(TimeValue.seconds(60))
                .jvmArgs("-ea")
                .build();
        try {
            new Runner(opt).run();
        } catch (RunnerException e) {
            e.printStackTrace();
        }
    }
}
