package findisland;

import actions.AvailableSymbols;
import actions.findislands.FindIslandGoThroughWidth;
import actions.findislands.FindIslandRecursive;
import actions.findislands.FindParallels;
import actions.findislands.IFindIsland;
import model.Model;
import model.SimpleModel;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;
import view.ConsoleViewer;
import view.IView;

import java.util.concurrent.TimeUnit;

/**
    Ih this bench we check area fill with *

    Look at ParallelFindingGoWidth.Having 4 thread we run 2.75 faster. To increase performance look at {@link FindingDependsOnBlockWidth}

    Benchmark                                                             Mode  Cnt     Score    Error  Units
    findisland.FindingFullEarthBigArea.ParallelFindingGoWidth.find        avgt    5    12,538 ±  1,391  ms/op
    findisland.FindingFullEarthBigArea.ParallelRecursiveFinding.find      avgt    5   807,413 ± 75,739  ms/op

    findisland.FindingFullEarthBigArea.SingleThreadFindingGoWidth.find    avgt    5    33,628 ±  0,752  ms/op
    findisland.FindingFullEarthBigArea.SingleThreadRecursiveFinding.find  avgt    5  2554,560 ± 78,157  ms/op
 */
public class FindingFullEarthBigArea {

    static int width = 5000;
    static int height = 5000;

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    public static class SingleThreadRecursiveFinding {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            IView view = new ConsoleViewer();
            findIsland = new FindParallels(view, model, new FindIslandRecursive(model), 1,64,64);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    @Fork(1)
    public static class SingleThreadFindingGoWidth {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            findIsland = new FindIslandGoThroughWidth(model);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    public static class ParallelRecursiveFinding {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            IView view = new ConsoleViewer();
            findIsland = new FindParallels(view, model, new FindIslandRecursive(model), 4,64,64);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    @State(Scope.Benchmark)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    @Timeout(time = 60)
    @Fork(1)
    public static class ParallelFindingGoWidth {

        private IFindIsland findIsland;

        @Setup(Level.Iteration)
        public void preSetup() throws InterruptedException {
            Model model = fillModel();
            IView view = new ConsoleViewer();
            findIsland = new FindParallels(view, model, new FindIslandGoThroughWidth(model), 4,64,64);
        }

        @Benchmark
        public int find() throws InterruptedException {
            return findIsland.findIsland(0, width, 0, height);
        }
    }

    private static Model fillModel(){
        SimpleModel model = new SimpleModel(width, height);

        for (int i = 0; i < model.getMaxHeight(); i++)
            for (int j = 0; j < model.getMaxWidth(); j++)
                model.set(j, i, AvailableSymbols.EARTH.getValue());

        return model;
    }

    public static void main(String[] args) {

        Options opt = new OptionsBuilder()
                .include(FindingFullEarthBigArea.class.getSimpleName())
                .warmupIterations(4)
                .measurementIterations(5)
                .forks(1)
                .timeout(TimeValue.seconds(60))
                .jvmArgs("-ea")
                .build();
        try {
            new Runner(opt).run();
        } catch (RunnerException e) {
            e.printStackTrace();
        }
    }
}
