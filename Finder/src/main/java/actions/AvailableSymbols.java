package actions;

/**
 * Available symbols
 */
public enum AvailableSymbols {

    WATER {
        @Override
        public char getValue() {
            return '~';
        }
    }, EARTH {
        @Override
        public char getValue() {
            return '*';
        }
    };

    /**
     * How symbol should view in array
     *
     * @return symbol view
     */
    public abstract char getValue();

    /**
     * Does provided symbol is available
     *
     * @param value provided symbol
     * @return true - available, false - other
     */
    public static boolean containsSymbols(char value) {
        for (AvailableSymbols entry : AvailableSymbols.values()) {
            if (entry.getValue() == value)
                return true;
        }
        return false;
    }
}
