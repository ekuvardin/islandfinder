package actions.findislands;

/**
 * Finding islands algorithms
 */
public enum TypeOfFinding {
    RECURSIVE {
        @Override
        public String getAbbreviation() {
            return "R";
        }
    }, WIDTHLINEGO {
        @Override
        public String getAbbreviation() {
            return "WL";
        }
    }, PARALLEL_RECURSIVE {
        @Override
        public String getAbbreviation() {
            return "PR";
        }
    }, PARALLEL_WIDTHLINEGO {
        @Override
        public String getAbbreviation() {
            return "PWL";
        }
    };

    /**
     * Get abbreviation
     *
     * @return abbreviation
     */
    public abstract String getAbbreviation();

    /**
     * Contains abbreviation
     *
     * @param value checked value
     * @return true - contain, false - other
     */
    public static boolean containsSymbols(String value) {
        for (TypeOfFinding entry : TypeOfFinding.values()) {
            if (entry.getAbbreviation().equals(value))
                return true;
        }
        return false;
    }

    /**
     * Get type by abbreviation
     *
     * @param value checked value
     * @return type if success else null
     */
    public static TypeOfFinding getType(String value) {
        for (TypeOfFinding entry : TypeOfFinding.values()) {
            if (entry.getAbbreviation().equals(value))
                return entry;
        }
        return null;
    }
}
