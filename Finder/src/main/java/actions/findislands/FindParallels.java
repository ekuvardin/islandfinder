package actions.findislands;

import actions.AvailableSymbols;
import model.Model;
import view.IView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Parallel algorithm
 */
public class FindParallels implements IFindIsland {

    protected int blockSizeW;
    protected int blockSizeH;
    protected int threadCount;
    protected IView view;
    protected Model model;
    protected IFindIsland singleThreadFindIsland;
    protected char sourceColour;

    public FindParallels(IView view, Model model, IFindIsland singleThreadFindIsland, int threadCount, int blockSizeW, int blockSizeH) {
        this.singleThreadFindIsland = singleThreadFindIsland;
        this.blockSizeW = blockSizeW;
        this.blockSizeH = blockSizeH;
        this.threadCount = threadCount;
        this.model = model;
        this.view = view;
        sourceColour = AvailableSymbols.EARTH.getValue();
    }

    @Override
    public int findIsland(int minWidth, int maxWidth, int minHeight, int maxHeight) {
        final ExecutorService executorPool =
                Executors.newFixedThreadPool(threadCount);

        try {
            final List<Callable<Integer>> partitions = initPartitions(minWidth, maxWidth, minHeight, maxHeight);
            return run(executorPool, partitions);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            view.showError(e);
            throw new RuntimeException(e);
        } finally {
            List<Runnable> runnables = executorPool.shutdownNow();
            if (runnables.size() != 0)
                view.showError(String.format("Not all tasks were terminated. Task count: %d", runnables.size()));
        }
    }

    private List<Callable<Integer>> initPartitions(int minWidth, int maxWidth, int minHeight, int maxHeight) {
        // If provided block size exceed height or width make it equal to them
        int sizeHeight = maxHeight - minHeight <= blockSizeH ? maxHeight - minHeight : blockSizeH;
        int sizeWidth = maxWidth - minWidth <= blockSizeW ? maxWidth - minWidth : blockSizeW;

        final int stepHeight = maxHeight - minHeight <= blockSizeH ? 1 : (maxHeight - minHeight) / blockSizeH;
        final int stepWidth = maxWidth - minWidth <= blockSizeW ? 1 : (maxWidth - minWidth) / blockSizeW;

        final List<Callable<Integer>> partitions =
                new ArrayList<>(stepHeight * stepWidth);

        for (int i = minHeight; i < maxHeight; i += sizeHeight) {
            final int indexHeight = i;
            for (int j = minWidth; j < maxWidth; j += sizeWidth) {
                final int indexWidth = j;
                partitions.add(() -> {
                    final int maxW = indexWidth + sizeWidth >= maxWidth ? maxWidth : indexWidth + sizeWidth;
                    final int maxH = indexHeight + sizeHeight >= maxHeight ? maxHeight : indexHeight + sizeHeight;

                    return singleThreadFindIsland.findIsland(
                            indexWidth,
                            maxW,
                            indexHeight,
                            maxH
                    )
                            + checkBorder(indexWidth, maxW, indexHeight, maxH);
                });
            }
        }

        return partitions;
    }

    private int run(ExecutorService executorPool, List<Callable<Integer>> partitions) throws InterruptedException, ExecutionException, TimeoutException {
        final List<Future<Integer>> resultFromParts =
                executorPool.invokeAll(partitions, 100000, TimeUnit.SECONDS);

        int res = 0;
        for (Future<Integer> future : resultFromParts) {
            res += future.get(100000, TimeUnit.SECONDS);
        }

        return res;
    }

    /**
     * We move from left bottom corner to left top corner to right top corner
     * Simply check neighbor coordinates and chain islands if they contact on the same line decrementing total islands count
     * ^---->
     * |****
     * |*
     * |*
     */
    private int checkBorder(int minWidth, int maxWidth, int minHeight, int maxHeight) {
        int islands = 0;
        int lastCellDecremented = -1;
        if (minWidth > 0) {
            for (int j = maxHeight - 1; j >= minHeight; j--) {
                if (model.get(minWidth - 1, j) == sourceColour && model.get(minWidth, j) == sourceColour) {
                    if (lastCellDecremented != j + 1) {
                        islands--;
                    }
                    lastCellDecremented = j;
                }
            }
        }

        // if last decremented was left top corner then going to right top corner we should take into account this fact
        // For example situation like this
        // |, - splits area to block border. In pictures above we see part of 4 blocks
        // ~ | *
        // -----
        // * | *
        // If we are in right bottom corner we decrement twice because we connect to ahead and left cell
        //
        // In this
        // * | *
        // -----
        // * | *
        // We shouldn't decrement twice -> check diagonal element(We know that islands can't have water inside)
        // -> then if in cell (0,0) would be <~> then we know that part (1,0) nad (0,1) can't connect together and surround (0,0)
        if (lastCellDecremented == minHeight && minHeight > 0 && model.get(minWidth - 1, minHeight - 1) == sourceColour)
            lastCellDecremented = minWidth - 1;
        else
            lastCellDecremented = minWidth - 2;

        if (minHeight > 0) {
            for (int j = minWidth; j < maxWidth; j++) {
                if (model.get(j, minHeight - 1) == sourceColour && model.get(j, minHeight) == sourceColour) {
                    if (lastCellDecremented != j - 1) {
                        islands--;
                    }
                    lastCellDecremented = j;
                }
            }
        }

        return islands;
    }
}
