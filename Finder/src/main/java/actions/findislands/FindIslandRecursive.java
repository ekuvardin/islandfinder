package actions.findislands;

import actions.AvailableSymbols;
import model.Model;

import java.util.HashSet;

/**
 * Recursive algorithm. May throw stackoverflow
 */
public class FindIslandRecursive implements IFindIsland {

    protected Model model;

    protected char sourceColour;

    public FindIslandRecursive(Model model) {
        this.model = model;
        sourceColour = AvailableSymbols.EARTH.getValue();
    }

    @Override
    public int findIsland(int minWidth, int maxWidth, int minHeight, int maxHeight) {
        Bound bound = new Bound(minWidth, maxWidth, minHeight, maxHeight);

        int islands = 0;

        HashSet<Integer> processed = new HashSet<>((maxHeight - minHeight) * (maxWidth - minWidth) / 2);

        for (int i = minHeight; i < maxHeight; i++)
            for (int j = minWidth; j < maxWidth; j++) {
                if (model.get(j, i) == sourceColour && !processed.contains(i * maxWidth + j)) {
                    islands++;
                    check(j, i, processed, bound);
                }
            }

        return islands;
    }

    private void check(int width, int height, HashSet<Integer> processed, Bound bound) {
        if (bound.checkInBound(width, height) && model.get(width, height) == sourceColour && !processed.contains(height * bound.maxWidth + width)) {
            processed.add(height * bound.maxWidth + width);

            // Check all neighbors
            if (!processed.contains(height * bound.maxWidth + width + 1))
                check(width + 1, height, processed, bound);

            if (!processed.contains(height * bound.maxWidth + width - 1))
                check(width - 1, height, processed, bound);

            if (!processed.contains((height + 1) * bound.maxWidth + width))
                check(width, height + 1, processed, bound);

            if (!processed.contains((height - 1) * bound.maxWidth + width))
                check(width, height - 1, processed, bound);
        }
    }

}