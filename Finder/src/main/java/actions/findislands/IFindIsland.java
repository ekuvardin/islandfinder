package actions.findislands;

/**
 * Common implementation to all finders
 */
public interface IFindIsland {

    /**
     * Find islands
     *
     * @param minWidth  minimal width
     * @param maxWidth  maximum width
     * @param minHeight minimal height
     * @param maxHeight maximum height
     * @return current islands count
     */
    int findIsland(int minWidth, int maxWidth, int minHeight, int maxHeight);
}
