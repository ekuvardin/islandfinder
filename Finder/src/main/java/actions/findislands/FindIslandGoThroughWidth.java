package actions.findislands;

import actions.AvailableSymbols;
import model.Model;

/**
 * The main algorithm
 *
 *
 * First iteration:
 * We ran out of cells in first line
 * finding pattern with continuous <*> each time increment islands
 *
 * while reach end of the line
 * 1. each time find pattern with several * and increment islands
 * 2. each time we compare current point with point ahead. If both are * then decrement islands
 *
 * Lets see example
 *  ~~~****~~~***~~
 *  *~~**~~~~~*~*~~
 *
 *  First iteration gives to us islands == 2 because we have two pattern of continuous *
 *
 *  Second
 *  [1][0] - * -> increment islands -> islands == 3
 *  check [0][0] -> no * continue
 *
 *  [1][1],[1][2] - ~ -> continue
 *
 *  [1][3] -> * -> increment islands -> islands == 4
 *  check [0][3]-> * -> decrement islands islands == 3
 *
 *  [1][4] -> * -> we have the same pattern the do nothing
 *  check [0][4] -> * -> on the last step we have already decrement island -> do nothing
 *
 *  [1][5],[1][6],[1][7],[1][8],[1][9] -> ~ -> do nothing
 *
 *  [1][10] -> * -> new pattern -> increment islands -> islands == 4
 *  check [0][10] -> * -> decrement islands islands == 3
 *
 *  [1][11] -> ~ -> do nothing
 *
 *  [1][12] -> * -> new pattern -> increment islands -> islands == 4
 *  check [0][12] -> * -> decrement islands islands == 3
 */
public class FindIslandGoThroughWidth implements IFindIsland {

    protected Model model;

    public FindIslandGoThroughWidth(Model model) {
        this.model = model;
    }

    @Override
    public int findIsland(int minWidth, int maxWidth, int minHeight, int maxHeight) {
        char sourceColour = AvailableSymbols.EARTH.getValue();
        int islands = 0;
        // First iteration
        for (int j = minWidth, lastFind = minWidth - 2; j < maxWidth; j++) {
            if (model.get(j, minHeight) == sourceColour) {
                if (lastFind != j - 1) {
                    // Increment islands each time we first meet EARTH
                    islands++;
                }
                lastFind = j;
            }
        }

        for (int i = minHeight + 1; i < maxHeight; i++)
            for (int j = minWidth, lastCellIncremented = minWidth - 2, lastCellDecremented = minWidth - 2; j < maxWidth; j++) {
                if (model.get(j, i) == sourceColour) {
                    if (lastCellIncremented != j - 1) {
                        // Increment islands each time we first meet EARTH
                        islands++;
                    }
                    lastCellIncremented = j;
                    if (model.get(j, i - 1) == sourceColour) {
                        if (lastCellDecremented != j - 1) {
                            // Increment islands each time we first meet EARTH ahead current coordinate
                            islands--;
                        }
                        lastCellDecremented = j;
                    }
                }
            }

        return islands;
    }
}
