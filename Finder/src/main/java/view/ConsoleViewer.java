package view;

import java.util.Arrays;

/**
 * Display commands to user
 */
public class ConsoleViewer implements IView {

    @Override
    public void showError(Exception e) {
        System.err.println("ERROR: " + Arrays.toString(e.getStackTrace()));
        System.err.println();
    }

    @Override
    public void showError(String message) {
        System.err.println("ERROR: " + message);
        System.err.println();
    }

    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }
}
