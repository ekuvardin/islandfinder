package runner;


import commands.ICommand;
import parser.IConsoleParser;
import parser.OptionParserImpl;
import view.ConsoleViewer;

/**
 * Main class. Start program.
 */
public class SimpleEditor {

    public static void main(String[] args) {
        IConsoleParser parser = new OptionParserImpl();
        ConsoleViewer view = new ConsoleViewer();
        view.printMessage("Welcome to simple find islands program!");
        ICommand command = parser.parseCommand(args);
        command.execute(view);
    }
}
