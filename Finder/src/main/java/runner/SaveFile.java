package runner;

import actions.AvailableSymbols;
import model.Model;
import model.SimpleModel;

import java.io.*;
import java.util.Scanner;

public class SaveFile {

    public static void main(String[] args){
        try(BufferedWriter out = new BufferedWriter(new FileWriter(new File("bigFile.txt")))){
            out.write("5000 5000");
            out.newLine();
            for (int i = 0; i < 5000; i++) {
                for (int j = 0; j < 5000; j++)
                    if (i == j)
                        out.write(AvailableSymbols.EARTH.getValue());
                    else
                        out.write(AvailableSymbols.WATER.getValue());
                out.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
