package model;

/**
 * Simple model to store point
 */
public class SimpleModel extends Model {

    private char[][] array;
    private final int maxWidth;
    private final int maxHeight;

    public SimpleModel(int width, int height) {
        this.array = new char[height][width];
        this.maxWidth = width;
        this.maxHeight = height;
    }

    @Override
    public void set(int width, int height, char value) {
        array[height][width] = value;
    }

    @Override
    public char get(int width, int height) {
        return array[height][width];
    }

    @Override
    public int getMaxWidth() {
        return maxWidth;
    }

    @Override
    public int getMaxHeight() {
        return maxHeight;
    }
}
