package model;

/**
 * Describe model where points are stored
 */
public abstract class Model {

    /**
     * Set new value to coordinates
     *
     * @param width  X-axis
     * @param height Y-axis
     * @param value  value
     */
    public abstract void set(int width, int height, char value);

    /**
     * Get value by coordinates
     *
     * @param width  X-axis
     * @param height Y-axis
     * @return value
     */
    public abstract char get(int width, int height);

    /**
     * Get width
     *
     * @return width
     */
    public abstract int getMaxWidth();

    /**
     * Get height
     *
     * @return height
     */
    public abstract int getMaxHeight();

    private static Model currentModel;

    /**
     * Get current model
     *
     * @return current model
     */
    public final static Model getCurrentModel() {
        return currentModel;
    }

    public static void setCurrentModel(Model currentModel) {
        Model.currentModel = currentModel;
    }
}
