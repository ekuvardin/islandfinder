package commands;

import commands.ICommand;
import view.IView;

import java.util.ArrayList;
import java.util.List;

public class ComposeCommand implements ICommand {

    private List<ICommand> commands = new ArrayList<>();

    @Override
    public void execute(IView view) {
        for(ICommand command : commands)
            command.execute(view);
    }

    public void add(ICommand command){
        commands.add(command);
    }
}
