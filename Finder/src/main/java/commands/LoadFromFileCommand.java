package commands;


import actions.AvailableSymbols;
import model.Model;
import model.SimpleModel;
import view.IView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Load file command
 */
public class LoadFromFileCommand implements ICommand {

    protected File file;

    public LoadFromFileCommand(File file) {
        this.file = file;
    }

    @Override
    public void execute(IView view) {
        try (Scanner scanner = new Scanner(file)) {
            int width;
            int height;

            if (scanner.hasNextInt()) {
                height = scanner.nextInt();
                if (height < 1 || height > 5000) {
                    view.showError("Height must be in range [0, 5000]");
                    return;
                }
            } else {
                view.showError("Height must be an integer");
                return;
            }

            if (scanner.hasNextInt()) {
                width = scanner.nextInt();
                if (width < 1 || width > 5000) {
                    view.showError("Width must be in range [0, 5000]");
                    return;
                }
            } else {
                view.showError("Width must be an integer");
                return;
            }
            scanner.nextLine();

            SimpleModel simpleModel = new SimpleModel(width, height);
            for (int i = 0; i < height; i++) {
                if (scanner.hasNextLine()) {
                    String res = scanner.nextLine();
                    if (res.length() != width) {
                        view.showError(String.format("Not enough values in line %d", i + 2));
                        return;
                    }

                    for (int j = 0; j < width; j++) {
                        char value = res.charAt(j);
                        if (!AvailableSymbols.containsSymbols(value)) {
                            view.showError(String.format("Not available symbol %s", value));
                            return;
                        }

                        simpleModel.set(j, i, res.charAt(j));
                    }
                } else {
                    view.showError(String.format("Height is to small. Expected %d, got %d", height, i));
                    return;
                }
            }
            Model.setCurrentModel(simpleModel);
        } catch (FileNotFoundException e) {
            view.showError("File not exists");
        }
    }
}
