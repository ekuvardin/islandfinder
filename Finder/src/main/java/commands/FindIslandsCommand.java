package commands;

import actions.findislands.*;
import model.Model;
import view.IView;

/**
 * Find island command
 */
public class FindIslandsCommand implements ICommand {

    private TypeOfFinding type;
    private int blockSizeW;
    private int blockSizeH;
    private int threadCount;

    public FindIslandsCommand(TypeOfFinding type) {
        this.type = type;
    }

    public FindIslandsCommand(TypeOfFinding type, int threadCount, int blockSizeW, int blockSizeH) {
        this(type);
        this.blockSizeW = blockSizeW;
        this.blockSizeH = blockSizeH;
        this.threadCount = threadCount;
    }

    @Override
    public void execute(IView view) {
        Model model = Model.getCurrentModel();
        if (model == null) {
            view.showError("Please first create area. Now it is empty.");
            return;
        }

        IFindIsland action = getAction(model, view);

        view.printMessage(String.format("Founded islands are %d", action.findIsland(0, model.getMaxWidth(), 0, model.getMaxHeight())));
    }

    protected IFindIsland getAction(Model model, IView view) {
        if (type == TypeOfFinding.RECURSIVE) {
            return new FindIslandRecursive(model);
        } else if (type == TypeOfFinding.PARALLEL_RECURSIVE) {
            return new FindParallels(view, model, new FindIslandRecursive(model), threadCount, blockSizeW, blockSizeH);
        } else if (type == TypeOfFinding.PARALLEL_WIDTHLINEGO) {
            return new FindParallels(view, model, new FindIslandGoThroughWidth(model), threadCount, blockSizeW, blockSizeH);
        } else {
            return new FindIslandGoThroughWidth(model);
        }
    }
}
