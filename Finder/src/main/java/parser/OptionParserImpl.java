package parser;

import actions.findislands.TypeOfFinding;
import commands.*;
import joptsimple.*;

import java.io.File;
import java.util.List;

/**
 * Option parser implementation
 */
public class OptionParserImpl implements IConsoleParser {

    private OptionParser parser;
    private OptionSpec<String> loadFromFile;
    private OptionSpec<String> algorithmType;
    private OptionSpec<Integer> threadCount;
    private OptionSpec<Integer> blockSizeWidth;
    private OptionSpec<Integer> blockSizeHeight;

    public OptionParserImpl() {
        this.parser = new OptionParser();
        OptionSpecBuilder loadFromFileBuilder = this.parser.accepts("ld", "Load from file");

        OptionSpecBuilder chooseAlgorithmType = this.parser.accepts("a", "Type of algorithm");

        this.loadFromFile = loadFromFileBuilder
                .withRequiredArg().ofType(String.class).describedAs("ld").required();

        this.algorithmType = chooseAlgorithmType.withRequiredArg().ofType(String.class).describedAs("a");
        this.threadCount = this.parser.accepts("t", "thread count").withRequiredArg().ofType(Integer.class).describedAs("t");
        this.blockSizeWidth = this.parser.accepts("bw", "block size width").withRequiredArg().ofType(Integer.class).describedAs("bw");
        this.blockSizeHeight = this.parser.accepts("bh", "block size height").withRequiredArg().ofType(Integer.class).describedAs("bh");
    }

    @Override
    public ICommand parseCommand(String[] args) {
        OptionSet set;
        try {
            set = parser.parse(args);
        } catch (OptionException e) {
            return new ErrorCommand(e);
        }
        List<?> nonOptions = set.nonOptionArguments();
        if (nonOptions != null && !nonOptions.isEmpty()) {
            return new ErrorCommand("Not recognized options " + nonOptions.toString());
        }

        ComposeCommand composeCommand = new ComposeCommand();
        composeCommand.add(new LoadFromFileCommand(new File(loadFromFile.value(set))));
        if (set.has(algorithmType)) {
            List<String> values = algorithmType.values(set);
            TypeOfFinding type = TypeOfFinding.getType(values.get(0));

            if (type == null) {
                return new ErrorCommand("Unknown type of finding algorithm. User -a {R,WL,PR,PWL}");
            } else if (type == TypeOfFinding.PARALLEL_RECURSIVE || type == TypeOfFinding.PARALLEL_WIDTHLINEGO) {
                int thread = 1;
                if (set.has(threadCount)) {
                    thread = threadCount.value(set);
                }

                int blockSizeW = Integer.MAX_VALUE;
                if (set.has(blockSizeWidth)) {
                    blockSizeW = blockSizeWidth.value(set);
                }

                int blockSizeH = Integer.MAX_VALUE;
                if (set.has(blockSizeHeight)) {
                    blockSizeH = blockSizeHeight.value(set);
                }

                if (blockSizeW <= 0 || thread <= 0 || blockSizeH <= 0)
                    return new ErrorCommand("task count and block size should be positive");

                composeCommand.add(new FindIslandsCommand(type, thread, blockSizeW, blockSizeH));
            } else {
                composeCommand.add(new FindIslandsCommand(type));
            }
        } else {
            composeCommand.add(new FindIslandsCommand(null));
        }
        return composeCommand;
    }
}