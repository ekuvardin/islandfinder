package integration.gowidth;

import actions.findislands.FindIslandGoThroughWidth;
import actions.findislands.IFindIsland;
import integration.FindIslandsCommonTests;
import model.Model;
import view.IView;

public class FindIslandGoThroughWidthTests extends FindIslandsCommonTests {

    @Override
    public IFindIsland createFinder(IView view, Model model) {
        return new FindIslandGoThroughWidth(model);
    }
}
