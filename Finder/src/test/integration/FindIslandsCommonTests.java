package integration;

import actions.AvailableSymbols;
import actions.findislands.IFindIsland;
import commands.ICommand;
import commands.LoadFromFileCommand;
import model.Model;
import model.SimpleModel;
import org.junit.Assert;
import org.junit.Test;
import view.ConsoleViewer;
import view.IView;

import java.io.File;

public abstract class FindIslandsCommonTests {

    protected IFindIsland findIsland;
    protected ConsoleViewer view = new ConsoleViewer();
    protected Model model;

    public abstract IFindIsland createFinder(IView view, Model model);

    @Test
    public void intersectionOnAllLines() {
        model = new SimpleModel(6, 7);
        findIsland = createFinder(view, model);
        String modelArray =
                "~~~~~~" +
                        "~~~~~~" +
                        "~~~~~~" +
                        "~~~~~~" +
                        "***~*~" +
                        "***~*~" +
                        "~~~~~~";

        check(0, 6, 0, 7, modelArray, 2);
    }

    @Test
    public void intersectionOnVertical() {
        model = new SimpleModel(6, 7);
        findIsland = createFinder(view, model);
        String modelArray =
                "~~~~~~" +
                        "~*~**~" +
                        "~***~~" +
                        "~~~~~~" +
                        "~~~~~~" +
                        "~~~~~~" +
                        "~~~~~~";

        check(0, 6, 0, 7, modelArray, 1);
    }

    @Test
    public void onlyOneIsland() {
        model = new SimpleModel(6, 3);
        findIsland = createFinder(view, model);
        String modelArray =
                "******" +
                        "******" +
                        "******";

        check(0, 6, 0, 3, modelArray, 1);
    }

    @Test
    public void noIsland() {
        model = new SimpleModel(6, 3);
        findIsland = createFinder(view, model);
        String modelArray =
                "~~~~~~" +
                        "~~~~~~" +
                        "~~~~~~";

        check(0, 6, 0, 3, modelArray, 0);
    }

    @Test
    public void islandOnBorder() {
        model = new SimpleModel(6, 3);
        findIsland = createFinder(view, model);
        String modelArray =
                "*~**~*" +
                        "*~~~~*" +
                        "***~**";

        check(0, 6, 0, 3, modelArray, 3);
    }

    @Test
    public void singlePointIsland() {
        model = new SimpleModel(6, 3);
        findIsland = createFinder(view, model);
        String modelArray =
                "~~~~~~" +
                        "~~*~~~" +
                        "~~~~~~";

        check(0, 6, 0, 3, modelArray, 1);
    }

    @Test
    public void islandOnDiagonalShouldNotCountedAsOne() {
        model = new SimpleModel(6, 3);
        findIsland = createFinder(view, model);
        String modelArray =
                "~*~~~~" +
                        "~~*~~~" +
                        "~~~*~~";

        check(0, 6, 0, 3, modelArray, 3);
    }

    @Test
    public void onePointAreaEarth() {
        model = new SimpleModel(1, 1);
        findIsland = createFinder(view, model);
        String modelArray =
                "*";

        check(0, 1, 0, 1, modelArray, 1);
    }

    @Test
    public void onePointAreaWater() {
        model = new SimpleModel(1, 1);
        findIsland = createFinder(view, model);
        String modelArray =
                "~";

        check(0, 1, 0, 1, modelArray, 0);
    }

    @Test
    public void controlExample() {
        model = new SimpleModel(6, 7);
        findIsland = createFinder(view, model);
        String modelArray =
                "~~~~~~" +
                        "~*~**~" +
                        "~***~~" +
                        "~~~~~~" +
                        "~*~~*~" +
                        "~**~*~" +
                        "~~~~~~";

        check(0, 6, 0, 7, modelArray, 3);
    }

    @Test
    public void bigFile() {
        model = new SimpleModel(64, 64);
        findIsland = createFinder(view, model);
        for (int i = 0; i < 64; i++)
            for (int j = 0; j < 64; j++)
                model.set(j, i, AvailableSymbols.EARTH.getValue());

        Assert.assertEquals(1, findIsland.findIsland(0, 64, 0, 64));
    }

    @Test
    public void readVeryBigFile() {
        ICommand command = new LoadFromFileCommand(new File("resources/bigFile.txt"));
        command.execute(view);
        model = Model.getCurrentModel();
        findIsland = createFinder(view, model);

        Assert.assertEquals(5000, findIsland.findIsland(0, 5000, 0, 5000));
    }

    protected void check(int minWidth, int maxWidth, int minHeight, int maxHeight, String modelArray, int expected) {
        for (int i = minHeight; i < maxHeight; i++)
            for (int j = minWidth; j < maxWidth; j++)
                model.set(j, i, modelArray.charAt(i * maxWidth + j));

        Assert.assertEquals(expected, findIsland.findIsland(minWidth, maxWidth, minHeight, maxHeight));
    }
}
