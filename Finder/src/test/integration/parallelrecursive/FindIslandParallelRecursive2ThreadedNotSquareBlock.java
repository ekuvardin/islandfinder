package integration.parallelrecursive;

import actions.findislands.FindIslandRecursive;
import actions.findislands.FindParallels;
import actions.findislands.IFindIsland;
import integration.FindIslandsCommonTests;
import model.Model;
import view.IView;

public class FindIslandParallelRecursive2ThreadedNotSquareBlock extends FindIslandsCommonTests {

    @Override
    public IFindIsland createFinder(IView view, Model model) {
        return new FindParallels(view, model, new FindIslandRecursive(model), 4, 2, 4);
    }
}
