package integration.parallelrecursive;

import actions.findislands.FindIslandRecursive;
import actions.findislands.FindParallels;
import actions.findislands.IFindIsland;

import integration.FindIslandsCommonTests;
import model.Model;
import org.junit.Ignore;
import org.junit.Test;
import view.IView;

public class FindIslandParallelRecursiveSingleThreaded extends FindIslandsCommonTests {

    @Override
    public IFindIsland createFinder(IView view, Model model) {
        return new FindParallels(view, model, new FindIslandRecursive(model), 1, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    @Ignore
    @Override
    @Test
    public void readVeryBigFile() {
        // May throw stackoverlfow
    }
}
