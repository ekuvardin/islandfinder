package integration.parallelrecursive;

import actions.findislands.FindIslandRecursive;
import actions.findislands.FindParallels;
import actions.findislands.IFindIsland;
import integration.FindIslandsCommonTests;
import model.Model;
import view.IView;

public class FindIslandParallelRecursiveSingleThreadedBatch extends FindIslandsCommonTests {

    @Override
    public IFindIsland createFinder(IView view, Model model) {
        return new FindParallels(view, model, new FindIslandRecursive(model), 1, 4, 4);
    }
}
