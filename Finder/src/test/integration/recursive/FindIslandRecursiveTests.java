package integration.recursive;

import actions.findislands.FindIslandRecursive;
import actions.findislands.IFindIsland;
import integration.FindIslandsCommonTests;
import model.Model;
import org.junit.Ignore;
import view.IView;

public class FindIslandRecursiveTests extends FindIslandsCommonTests {

    @Override
    public IFindIsland createFinder(IView view, Model model) {
        return new FindIslandRecursive(model);
    }

    @Ignore
    @Override
    public void readVeryBigFile() {
        // May throw stackoverlfow
    }
}
