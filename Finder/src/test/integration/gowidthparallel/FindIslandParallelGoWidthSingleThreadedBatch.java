package integration.gowidthparallel;

import actions.findislands.*;
import integration.FindIslandsCommonTests;
import model.Model;
import model.SimpleModel;
import org.junit.Test;
import view.IView;

public class FindIslandParallelGoWidthSingleThreadedBatch extends FindIslandsCommonTests {

    @Override
    public IFindIsland createFinder(IView view, Model model) {
        return new FindParallels(view, model, new FindIslandGoThroughWidth(model), 1, 4, 4);
    }

    @Test
    public void noInterSectionOnDiagonal() {
        model = new SimpleModel(5, 5);
        findIsland = createFinder(view, model);
        String modelArray =
                "~~~~~" +
                        "~~~~~" +
                        "~~~~~" +
                        "~~~~*" +
                        "~~~**";

        check(0, 5, 0, 5, modelArray, 1);
    }

    @Test
    public void interSectionOnDiagonal() {
        model = new SimpleModel(5, 5);
        findIsland = createFinder(view, model);
        String modelArray =
                "~~~~~" +
                        "~~~~~" +
                        "~~~~~" +
                        "~~~**" +
                        "~~~**";

        check(0, 5, 0, 5, modelArray, 1);
    }

}
