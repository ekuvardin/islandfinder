package integration.gowidthparallel;

import actions.findislands.*;
import integration.FindIslandsCommonTests;
import model.Model;
import view.IView;

public class FindIslandParallelGoWidthSingleThreaded extends FindIslandsCommonTests {

    @Override
    public IFindIsland createFinder(IView view, Model model) {
        return new FindParallels(view, model, new FindIslandGoThroughWidth(model), 1, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }
}
