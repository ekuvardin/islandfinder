package integration.gowidthparallel;

import actions.findislands.FindIslandGoThroughWidth;
import actions.findislands.FindParallels;
import actions.findislands.IFindIsland;
import integration.FindIslandsCommonTests;
import model.Model;
import view.IView;

public class FindIslandParallelGoWidth2ThreadedNotSquareBlock extends FindIslandsCommonTests {

    @Override
    public IFindIsland createFinder(IView view, Model model) {
        return new FindParallels(view, model, new FindIslandGoThroughWidth(model), 4, 2, 4);
    }
}
