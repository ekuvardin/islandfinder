package unit.actions.findislands;

import actions.AvailableSymbols;
import actions.findislands.FindIslandRecursive;
import model.Model;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class FindIslandRecursiveTests {

    Model model;
    FindIslandRecursive findIslandRecursive;

    @Before
    public void preTest() {
        model = Mockito.mock(Model.class);
        findIslandRecursive = new FindIslandRecursive(model);
    }

    @Test
    public void findIslandShouldReturnOne() {
        Mockito.when(model.get(0,0)).thenReturn(AvailableSymbols.EARTH.getValue());
        Mockito.when(model.get(1,0)).thenReturn(AvailableSymbols.EARTH.getValue());
        Mockito.when(model.get(0,1)).thenReturn(AvailableSymbols.WATER.getValue());
        Mockito.when(model.get(1,1)).thenReturn(AvailableSymbols.WATER.getValue());

        Assert.assertEquals(1, findIslandRecursive.findIsland(0, 2, 0, 2));
    }

    @Test
    public void findIslandShouldReturnTwo() {
        Mockito.when(model.get(0,0)).thenReturn(AvailableSymbols.EARTH.getValue());
        Mockito.when(model.get(0,1)).thenReturn(AvailableSymbols.WATER.getValue());
        Mockito.when(model.get(1,1)).thenReturn(AvailableSymbols.EARTH.getValue());
        Mockito.when(model.get(1,0)).thenReturn(AvailableSymbols.WATER.getValue());

        Assert.assertEquals(2, findIslandRecursive.findIsland(0, 2, 0, 2));
    }

    @Test
    public void findIslandShouldReturnOneFillEarth() {
        Mockito.when(model.get(0,0)).thenReturn(AvailableSymbols.EARTH.getValue());
        Mockito.when(model.get(0,1)).thenReturn(AvailableSymbols.EARTH.getValue());
        Mockito.when(model.get(1,1)).thenReturn(AvailableSymbols.EARTH.getValue());
        Mockito.when(model.get(1,0)).thenReturn(AvailableSymbols.EARTH.getValue());

        Assert.assertEquals(1, findIslandRecursive.findIsland(0, 2, 0, 2));
    }

    @Test
    public void findIslandShouldReturnOneCorner() {
        Mockito.when(model.get(0,0)).thenReturn(AvailableSymbols.WATER.getValue());
        Mockito.when(model.get(0,1)).thenReturn(AvailableSymbols.EARTH.getValue());
        Mockito.when(model.get(1,1)).thenReturn(AvailableSymbols.EARTH.getValue());
        Mockito.when(model.get(1,0)).thenReturn(AvailableSymbols.EARTH.getValue());

        Assert.assertEquals(1, findIslandRecursive.findIsland(0, 2, 0, 2));
    }
}
