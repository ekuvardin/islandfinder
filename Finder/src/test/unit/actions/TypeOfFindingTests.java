package unit.actions;

import actions.findislands.TypeOfFinding;
import org.junit.Assert;
import org.junit.Test;

public class TypeOfFindingTests {

    @Test
    public void TypeOfFindingParallelRecursiveShouldReturnCorrectAbbreviation(){
        Assert.assertEquals("PR",  TypeOfFinding.PARALLEL_RECURSIVE.getAbbreviation());
    }

    @Test
    public void TypeOfFindingRecursiveShouldReturnCorrectAbbreviation(){
        Assert.assertEquals("R",  TypeOfFinding.RECURSIVE.getAbbreviation());
    }

    @Test
    public void TypeOfFindingParallelWidthLineGoShouldReturnCorrectAbbreviation(){
        Assert.assertEquals("PWL",  TypeOfFinding.PARALLEL_WIDTHLINEGO.getAbbreviation());
    }

    @Test
    public void TypeOfFindingWidthLineGoShouldReturnCorrectAbbreviation(){
        Assert.assertEquals("WL",  TypeOfFinding.WIDTHLINEGO.getAbbreviation());
    }

    @Test
    public void containsSymbolsShouldReturnWidthLineGo(){
        Assert.assertTrue( TypeOfFinding.containsSymbols("WL"));
    }

    @Test
    public void containsSymbolsShouldReturnParallelRecursive(){
        Assert.assertTrue(  TypeOfFinding.containsSymbols("PR"));
    }

    @Test
    public void containsSymbolsShouldReturnRecursive(){
        Assert.assertTrue( TypeOfFinding.containsSymbols("R"));
    }

    @Test
    public void containsSymbolsShouldReturnParallelWidthLineGo(){
        Assert.assertTrue(TypeOfFinding.containsSymbols("PWL"));
    }

    @Test
    public void containsSymbolsShouldReturnFalse(){
        Assert.assertFalse(TypeOfFinding.containsSymbols("PdWL"));
    }

    @Test
    public void getTypeShouldReturnParallelRecursive(){
        Assert.assertEquals(TypeOfFinding.PARALLEL_RECURSIVE,  TypeOfFinding.getType("PR"));
    }

    @Test
    public void getTypeShouldReturnRecursive(){
        Assert.assertEquals(TypeOfFinding.RECURSIVE,   TypeOfFinding.getType("R"));
    }

    @Test
    public void getTypeShouldReturnParallelWidthLineGo(){
        Assert.assertEquals(TypeOfFinding.PARALLEL_WIDTHLINEGO,   TypeOfFinding.getType("PWL"));
    }

    @Test
    public void getTypeShouldReturnWidthLineGo(){
        Assert.assertEquals(TypeOfFinding.WIDTHLINEGO, TypeOfFinding.getType("WL"));
    }

    @Test
    public void getTypeShouldReturnNull(){
        Assert.assertNull(TypeOfFinding.getType("WTL"));
    }
}
